#include <stdlib.h>
#include <stdio.h>
#include "triangle.h"

void allocateNumberTriangle(const int height, int ***triangle)
{
	*triangle = (int **) malloc(height * sizeof(int*));
	for(int i = 0; i < height; i++){
		(*triangle)[i] = (int *) malloc(9 * sizeof(int));
	}
}

void initializeNumberTriangle(const int height, int **triangle)
{
	int triangle_5x9[5][9] = {
	        {-1,-1,-1,-1,0,-1,-1,-1,-1},
	        {-1,-1,-1,0,1,2,-1,-1,-1},
	        {-1,-1,0,1,2,3,4,-1,-1},
	        {-1,0,1,2,3,4,5,6,-1},
        	{0,1,2,3,4,5,6,7,8}
	};

	for(int i = 0; i < height; i++){
		for(int j = 0; j < 9; j++){
			triangle[i][j] = triangle_5x9[i][j];			
		}
	}
}

void printNumberTriangle(const int height, int **triangle)
{
	for(int i = 0; i < height; i++){
		for(int j = 0; j < 9; j++){
			if(triangle[i][j] > -1){
				printf("%d ", triangle[i][j]);
			}else{
				printf("  ");
			}
		}
		printf("\n");
	}
}

void deallocateNumberTriangle(const int height, int **triangle)
{
	for(int i = 0; i < height; i++){
		free(triangle[i]);
	}
	free(triangle);
}
