#include <stdio.h>
#include "triangle.h"


void clearInputBuffer() {
  while ( getchar() != '\n' );
}

int getInt(const char* msg, int low, int high) {
	/* Keep prompting the user for input until they enter a single integer
	 * whose value is between low and high (inclusive).
	 */
	int numInts = 0, num = 0;
	while (numInts != 1 || num < low || num > high) {
	    printf("%s",msg);
	    numInts = scanf("%d", &num);  // returns the number of integers read from input
	    clearInputBuffer();
	}
	return num;
}

int main(int argc, const char * argv[])
{
	int size;
	size = getInt("Please enter the height of the triangle [1-5]: ", 1, 5);	
//	print5Triangle();
	int **triangle = NULL;
	allocateNumberTriangle((const int)size,&triangle);
	initializeNumberTriangle((const int)size,triangle);
	printNumberTriangle((const int)size,triangle);
	deallocateNumberTriangle((const int)size,triangle);		
	return(0);
}
