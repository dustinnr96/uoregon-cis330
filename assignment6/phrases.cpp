#include <sstream>     	// std::istringstream
#include <fstream>
#include <map>		// std::multimap
#include <algorithm>   	// std::copy, std::for_each
#include <cctype>

#include "mr.hpp"
#include "ioutils.hpp"
#include "phrases.hpp"

inline void strfix(std::string& word){
	bool seen_letter=false;
	auto end = std::remove_if(word.begin(),word.end(),
		[&seen_letter](char c){
			bool punct = std::ispunct(c);
			if(seen_letter){
				return false;
			}else if(!punct){
				seen_letter = true;
				return false;
			}else{
				return punct;
			}

	});
	seen_letter = false;
	auto begin = std::remove_if(word.rbegin(),word.rend(),
		[&seen_letter](char c){
			bool punct = std::ispunct(c);
			if(seen_letter){
				return false;
			}else if(!punct){
				seen_letter = true;
				return false;
			}else{
				return punct;
			}
	});
	word = std::string(begin.base(),end);
}

namespace mr {
// Map and reduce methods to count occurrences of a word in a given text.

// A specialized map function with string keys and int values
void
Phrases::MRmap(const std::map<std::string,std::string> &input,
				std::multimap<std::string,int> &out_values) {
	IOUtils io;
	// input: in a real Map Reduce application the input will also be a map
	// where the key is a file name or URL, and the value is the document's contents.
	// Here we just take the string input and process it.
	for (auto it = input.begin(); it != input.end(); it++ ) {
		std::string inputString = io.readFromFile(it->first);

		// Split up the input into words
		std::istringstream iss(inputString);
		std::string phrase;
		std::string prev;
		do {
			int counter = 0;
			std::string word1;
			std::string word2;
			if(prev.length() > 0){
				word1 = prev;
			}else{
				iss >> word1;
			}
			if(iss){
				iss >> word2;
				strfix(word1);
				strfix(word2);
				phrase = word1 + " " + word2;
				out_values.insert(std::pair<std::string,int>(phrase,1));
				prev = word2;
			}else{
				break;
			}
		} while (iss);
	}
}

// A specialized reduce function with string keys and int values
void
Phrases::MRreduce(const std::multimap<std::string,int> &intermediate_values,
					std::map<std::string,int> &out_values) {

	// Sum up the counts for all intermediate_values with the same key
	// The result is the out_values map with each unique word as
	// the key and a total count of occurrences of that word as the value.
	std::for_each(intermediate_values.begin(), intermediate_values.end(),
			// Anonymous function that increments the sum for each unique key (word)
			[&](std::pair<std::string,int> mapElement)->void
			{
				out_values[mapElement.first] += 1;
			});  // end of for_each
}

}; // namespace mr
