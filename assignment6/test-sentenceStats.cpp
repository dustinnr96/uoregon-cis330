/*
 * test-wordCount.cpp
 *
 * Testing the word count MR problem.
 *
 *  Created on: Februrary 16, 2014
 *      Author: norris
 */


#include <iostream>
#include <string>
#include <future>       // std::async, std::future, std::launch
#include <thread>       // std::this_thread::sleep_for
#include <chrono>       // std::chrono (timing)
#include <climits>

#include "ioutils.hpp"
#include "mr.hpp"
#include "sentenceStats.hpp"


int main(int argc, const char *argv[]) {
	using namespace std;

	// Use MapReduce to compute a word count
	mr::SentenceStats sentenceStats;
	std::map<string,int> final;

	// Assume that the URLs to process are listed (one per line)
	// a file called wordCount-input.txt in the current directory
	IOUtils io;
	io.openStream(argc,argv);
	vector<string> fileNames = io.split(io.readFromStream(),'\n');
	io.closeStream();

	auto start = std::chrono::steady_clock::now(); // start timer

#ifndef PARALLEL_MR
	// Invoke the Map Reduce runtime
	mr::run(sentenceStats, fileNames, final);
#else
	// To run the parallel Map Reduce, include -DPARALLEL_MR to
	// the CXXFLAGS variable in the Makefile,
	// then make clean and make
	mr::prun<string,int>(wordCount, fileNames, final, 10, 4);
#endif

	auto end = std::chrono::steady_clock::now();

	// Print the final results
	for (auto it = final.begin(); it != final.end(); it++ )
		cout << it->first << ": " << it->second << endl;


	string max_sent;
	string min_sent;
	int max_len = 0;
	int min_len = INT_MAX;
	float num_sentences = 0;
	float len_sum = 0;
	vector<int> lengths;
	for (const auto& kv: final){
		//word = std::regex_replace(word,punct_re
		istringstream iss(kv.first);
		int len = -1;
		do {
			string word;
			iss >> word;
			len++;
		} while (iss);
		if(len > max_len){
			max_len = len;
			max_sent = kv.first;
		}
		if(len < min_len){
			min_len = len;
			min_sent = kv.first;
		}
		len_sum += (len * kv.second);
		num_sentences += kv.second;
	}
	float avg_len = len_sum/num_sentences;
	cout << "Max sentence length: " << max_len << endl;
	cout << max_sent << endl;
	cout << "Min sentence length: " << min_len << endl;
	cout << min_sent << endl;
	cout << "Average sentence length: " << avg_len << endl;
	auto diff = end - start;
	cout << "MapReduce time: " << std::chrono::duration <double, std::milli> (diff).count() << " ms" << endl;
	return 0;
}
