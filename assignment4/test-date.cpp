#include "ioutils.hpp"
#include "datecipher.hpp"


int main(int argc, const char * argv[]){
/*	DateCipher *c = new DateCipher();

	std::string message = "elephant";
	std::cout << (message + "\n");
	std::string encrypted = c->encrypt(message);
	std::cout << "Encrypted: "  << (encrypted + "\n");
	std::string decrypted = c->decrypt(encrypted);
	std::cout << "Decrypted: " << (decrypted + "\n");
*/
        IOUtils io;
        io.openStream(argc,argv);
        std::string input, encrypted, decrypted;
        input = io.readFromStream();
        std::cout << "Original text:" << std::endl << input;

        DateCipher d;
        encrypted = d.encrypt(input);
        std::cout << "Encrypted text:" << std::endl << encrypted;

        decrypted = d.decrypt(encrypted);
        std::cout << "Decrypted text:" << std::endl << decrypted;

        if (decrypted == input) std::cout << "Decrypted text matches input!" << std::endl;
        else {
                std::cout << "Oops! Decrypted text doesn't match input!" << std::endl;
                return 1;   // Make sure to return a non-zero value to indicate failure
        }
        return 0;	
	
	return 0;
}
