#ifndef DATECIPHER_HPP_
#define DATECIPHER_HPP_

#include <string>
#include "cipher.hpp"

class DateCipher : Cipher {
public:
	DateCipher() : Cipher::Cipher(){};
	virtual ~DateCipher();
	
	std::string encrypt( std::string &text ) ;
	std::string decrypt ( std::string &text ) ;
private:
//	static const std::string key = "04171991";
	/* A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
	   0 4 1 7 1 9 9 1 0 4 1 7 1 9 9 1 0 4 1 7 1 9 9 1 0 4 */
	 int key [26] = {0,4,1,7,1,9,9,1,0,4,1,7,1,9,9,1,0,4,1,7,1,9,9,1,0,4};
	int date[8] = {0,4,1,7,1,9,9,1};
};

#endif

