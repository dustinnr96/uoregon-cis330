#include "datecipher.hpp"
#include <iostream>	
       std::string DateCipher::encrypt( std::string &text ){
		std::string  encrypted = text;
		int in,out,lowercase;
	

		int i = 0;
		for ( std::string::iterator it=encrypted.begin(); it!=encrypted.end(); ++it){
			int ord = (int) *it;
                       	if(!((ord > 64 && ord < 91 ) || (ord > 96 && ord < 123))){
				continue;
			} 

			 if((int) *it > 96){
                                lowercase = 32;
                        }else{
                                lowercase = 0;
                        }
			in = ((int) *it) - (65 + lowercase);
			//int shift = 
			int shift = this->date[i];
			i = (++i % 8);
			out = (in + shift) % 26;
			*it = char(out + (65 + lowercase));
		}	
		return encrypted;

		
	};

        std::string DateCipher::decrypt ( std::string &text ){
                int in,out,lowercase;
		std::string decrypted = text;
               
                int i = 0;
                for ( std::string::iterator it=decrypted.begin(); it!=decrypted.end(); ++it){
                        int ord = (int) *it;
                        if(!((ord > 64 && ord < 91 ) || (ord > 96 && ord < 123))){
                                continue;
                        }

                         if((int) *it > 96){
                                lowercase = 32;
                        }else{
                                lowercase = 0;
                        }
                        in = ((int) *it) - (65 + lowercase);
                        int shift = this->date[i];
//                        out = (in - shift) % 26;
			out = ((in + 26) - (this->date[i]))%26;
			i = (++i % 8);
                        *it = char(out + (65 + lowercase));
                }

                return decrypted;
	};

	DateCipher::~DateCipher(){

	};
