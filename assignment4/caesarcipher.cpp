#include "caesarcipher.hpp"
       std::string CaesarCipher::encrypt( std::string &text ){
		
		int in,out,lowercase;
		std::string encrypted = text;
		for ( std::string::iterator it=encrypted.begin(); it!=encrypted.end(); ++it){
                        int ord = (int) * it;
			if(ord != 32 && !((ord > 64 && ord < 91 ) || (ord > 96 && ord < 123))){
                                continue;
                        }

			if(ord > 96){
				lowercase = 32;
			}else{
				lowercase = 0;
			}	

			if(*it == ' '){
				in = 27;
			}else{
		
				in = (ord) - (64+lowercase);
			}
			out = (in + this->key) % 27;
			if(out == 0){
				*it = ' ';
			}else	{
				*it = char(out + (64+lowercase));
			}
		}
		return encrypted;
	};

        std::string CaesarCipher::decrypt ( std::string &text ){
		int in,out,lowercase;
		std::string decrypted = text;
                for ( std::string::iterator it=decrypted.begin(); it!=decrypted.end(); ++it){
			int ord = (int) * it;
                        if(ord != 32 && !((ord > 64 && ord < 91 ) || (ord > 96 && ord < 123))){
                                continue;
                        }


			if(ord > 96){
				lowercase = 31;
			}else{
				lowercase = 0;
			}	
                	if(*it == ' '){
				in = 27;
			}else{
				in = ord - (64+lowercase);
			}
			out = ((in+27) - this->key) % 27;
			if(out == 0){
				*it = ' ';
			}else{
				*it = char(out + (64+lowercase));
			}
		}
		return decrypted;
	};

	CaesarCipher::~CaesarCipher(){

	};
