#ifndef CAESARCIPHER_HPP_
#define CAESARCIPHER_HPP_
#include <iostream>
#include <string>
#include "cipher.hpp"

class CaesarCipher : Cipher {
public:
	CaesarCipher() : Cipher::Cipher(){};
	virtual ~CaesarCipher();	
	std::string encrypt( std::string &text );
	std::string decrypt ( std::string &text );
private:
	static const int key = 3;
};

#endif

