#include <cstdlib>
#include <vector>

#include "cell.hpp"

//enum Tile { EMPTY, FARMER, SHEEP, WOLF };
class Game {
public:
	Game(int x, int y);
	void generate_grid();
	void transition();
	void print();
	~Game();
	Cell*  getCell(int x, int y);
private:
	std::vector<std::vector<Cell> >  grid;
	
	std::vector<Cell> get_neighbors(int i, int j);
};		
