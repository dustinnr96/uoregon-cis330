#include "cell.hpp"

	Cell::Cell(){

	}
	
	Cell::Cell(int x, int y, Tile type){
		this->x = x;
		this->y = y;
		this->_type = type;
	}

	Cell::~Cell(){

	}	

	Tile Cell::type(){
		return this->_type;
	}

	void Cell::type(const Tile& type){
		this->_type=type;
	}
	
	void Cell::print(){
		char out;
		if(this->type() == EMPTY){
			out = '-';
		}else if(this->type() == SHEEP){
			out = 'S';
		}else if(this->type() == WOLF){
			out = 'W';
		}else if(this->type() == FARMER){
			out = 'F';
		}
		std::cout <<  out;
	}

        std::vector<Cell*> Cell::get_neighbors(std::vector<std::vector<Cell> >& grid, std::vector<Cell*>& neighbors){
                //td::vector<Cell*> neighbors;
		int i = this->y, j = this->x;
                int size = grid.size();
	//	std::cout << "i-1,j-1\n";
                (i>0 && j>0) ? neighbors.push_back(&grid[i-1][j-1]) : void();
          //      std::cout << "i-1,j\n";
		(i>0) ? neighbors.push_back(&grid[i-1][j]) : void();
	//	std::cout << "i-1,j+1 \n";
                (i>0 && j<(size-1)) ? neighbors.push_back(&grid[i-1][j+1]) : void();
	//	std::cout << "i,j-1\n";
                (j>0) ? neighbors.push_back(&grid[i][j-1]) : void();
	//	std::cout << "i,j+1\n";
                (j<(size-1)) ? neighbors.push_back(&grid[i][j+1]) : void();
	//	std::cout << "i+1,j-1\n";
                (i<(size-1) && j>0) ? neighbors.push_back(&grid[i+1][j-1]) : void();
	//	std::cout << "i+1,j\n";
                (i<(size-1)) ? neighbors.push_back(&grid[i+1][j]) : void();
               // std::cout << "i+1,j+1\n";
		(i<(size-1) && j<(size-1)) ? neighbors.push_back(&grid[i+1][j+1]) : void();
                return neighbors;
        };

	bool Cell::is_empty(){
		return this->type() == EMPTY;
	}

	void Cell::act(std::vector<std::vector<Cell> >& grid){
		std::vector<Cell*> n;
		this->get_neighbors(grid,n);
		int counts[4] = {0,0,0,0};
        	for(int i = 0; i < n.size(); i ++){
			Tile type = n[i]->type();
			
        	        counts[(int)type] = counts[(int)type]+1;
                }
                if(this->type() == EMPTY){
         	       if(counts[SHEEP] >= 2){
                	       this->type(SHEEP);
                        }else if(counts[WOLF] >= 2){
                        	this->type(WOLF);
                        }else if(counts[FARMER] >= 2){
	        		this->type(FARMER);
                        }
                }else if(this->type() == FARMER){
                        if(counts[EMPTY]>0){
                                std::vector<Cell*> empty;
				for(int i = 0; i < n.size(); i++){
					if(n[i]->type() == EMPTY){
						empty.push_back(n[i]);
					}
				}
				int new_pos = rand() % empty.size();
				empty[new_pos]->type(FARMER);
                       		this->type(EMPTY);
			 }        
                }else if(this->type() == WOLF){
                	if(counts[FARMER] >= 1){
				this->type(EMPTY);
			}else if(counts[WOLF] >= 3){
				this->type(EMPTY);
			}else if(counts[SHEEP] == 0){
				this->type(EMPTY);
			
			}
                }else if(this->type() == SHEEP){
                        if(counts[WOLF] >= 1){
                              this->type(EMPTY);
                        }else if(counts[SHEEP] >= 3){
                                this->type(EMPTY);
                        }
                }
	}
