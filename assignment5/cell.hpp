#include <vector>
#include <algorithm>
#include <iostream>
enum Tile { EMPTY, FARMER, SHEEP, WOLF };

class Cell {
public:
	Cell();
	Cell(int x, int y, Tile type);
	void act(std::vector<std::vector<Cell> >& grid);
	Tile type();
	void type(const Tile& type);
	bool is_empty();
	void print();
	~Cell();
private:
	int x, y;
	Tile _type;
	std::vector<Cell*> get_neighbors(std::vector<std::vector<Cell> >& grid,  std::vector<Cell*>& neighbors);
};		
