#include <climits>
#include "game.hpp"


int getInt(int low, int high) {
	/* Keep prompting the user for input until they enter a single integer
	 * whose value is between low and high (inclusive).
	 */
	int num = -1;
	while (num < low || num > high) {
	    std::cin >> num;
	}
	return num;
}

int main(int argc, char ** argv){
	srand (time(NULL));
	
	
	std::cout << "Please enter the size of the grid: (int int): ";
	int x =getInt ( 0, INT_MAX);
	int y = getInt(0,INT_MAX);
	std::cout << "Please enter the number of steps: (int): ";
	int steps = getInt(0,INT_MAX);
	Game g = Game(x,y);	
	g.generate_grid();
	for(int i = 0; i < steps; i ++ ){
		std::cout << i+1 << "." << std::endl;
		g.print();
		g.transition();
	}

	return 0;
}	
