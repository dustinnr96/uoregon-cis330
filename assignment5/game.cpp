#include "game.hpp"

	Game::Game(int x, int y){
		this->grid = std::vector<std::vector<Cell> >(y);
		for(int i = 0;i < y; i++){
			this->grid[i].resize(x);
			for(int j = 0;j< x;j++){
				this->grid[i][j] = Cell(j,i,EMPTY);
			}
		}	
	}
	
	void Game::generate_grid(){
		int farmcount =0, sheepcount=0, wolfcount=0;
		for(int i = 0; i < this->grid.size(); i ++){
			for(int j = 0; j < this->grid[i].size(); j ++){
				Tile type = Tile(rand() % 4);
				this->grid[i][j].type(type);
			}
		}

	}

        Cell* Game::getCell(int x, int y){
		return &this->grid[y][x];
	}

	void Game::print(){
		for(int i = 0; i < this->grid.size(); i ++){
			for(int j =0; j < this->grid[i].size(); j ++){
				this->grid[i][j].print();
			}
			std::cout << "\n";
		}
		for(int i = 0; i < this->grid[0].size(); i++){
			std::cout << "=";
		}
		std::cout << std::endl;
	}
	std::vector<Cell> Game::get_neighbors(int i, int j){
		std::vector<Cell> neighbors;

		int size = this->grid.size();	
		(i>0 && j>0) ? neighbors.push_back(this->grid[i-1][j-1]) : void();
		(i>0) ? neighbors.push_back(this->grid[i-1][j]) : void();
		(i>0 && j<size) ? neighbors.push_back(this->grid[i-1][j+1]) : void();
		(j>0) ? neighbors.push_back(this->grid[i][j-1]) : void();
		(j<size) ? neighbors.push_back(this->grid[i][j+1]) : void();
		(i<size && j>0) ? neighbors.push_back(this->grid[i+1][j-1]) : void();
		(i<size) ? neighbors.push_back(this->grid[i+1][j]) : void();
		(i<size && j<size) ? neighbors.push_back(this->grid[i+1][j+1]) : void();
		return neighbors;
	};

	void Game::transition(){
		std::vector<Cell*> sorted[4];		
                for(int i = 0; i < this->grid.size(); i ++){
                        for(int j = 0; j < this->grid[i].size(); j ++){
				sorted[this->grid[i][j].type()].push_back(&this->grid[i][j]); 
			}
                }		
		for(int i = 0; i < sorted[WOLF].size(); i++){
			sorted[WOLF][i]->act(this->grid);
		}
		for(int i = 0; i < sorted[SHEEP].size(); i++){
			sorted[SHEEP][i]->act(this->grid);
		}
		for(int i = 0; i < sorted[FARMER].size(); i++){
			sorted[FARMER][i]->act(this->grid);
		}
		for(int i = 0; i < sorted[EMPTY].size(); i++){
			sorted[EMPTY][i]->act(this->grid);
		}
	}


	Game::~Game(){
	}

