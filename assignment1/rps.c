    #include <stdio.h>
    #include <stdlib.h>
    #include <time.h>      
    #include <string.h>
     
    char* getUserChoice(char * choice) {
        /* Prompt the user "Enter rock, paper, or scissors: " and return
           the string they enter */
	char * temp = (char *) malloc(9*sizeof(char));
	int c=0;
	while(1){
		printf("\nEnter rock, paper, or scissors: \n"); 	
		fgets(choice,9,stdin);
		strcpy(temp,choice);
		if(strcmp(choice,"scissors") == 0){
			break;
		}else{choice[5]='\0';}
		if(strcmp(choice,"paper") == 0){
			break;
		}else{choice[4]='\0';}
		if(strcmp(choice,"rock") == 0){
			break;
		}
		strcpy(choice,"");
		if(strlen(temp) >= 9){
			while((c = getchar()) != '\n' && c != EOF){
		/*	continue;*/
			}
		}	
		
		printf("invalid choice, try again\n");
		
	}
		free(temp);
		return choice;
    }
     
    char* getComputerChoice() {
        srand (time(NULL));
        /* get a pseudo-random integer between 0 and 2 (inclusive) */
        int randChoice = rand() % 3;
    	char * computerChoice = (char *) malloc(8 * sizeof(char)); 
        /* If randChoice is 0, return "rock"; if randChoice is 1,
         return "paper", and if randChoice is 2, return "scissors". */
    	if(randChoice == 0){
		return strcpy(computerChoice,"rock");
	}else if(randChoice == 1){
		return strcpy(computerChoice,"paper");
	}else{
		return strcpy(computerChoice,"scissors");
	} 
    }
     
    char* compare(char* choice1, char* choice2)
    {
        /* Implement the logic of the game here. If choice1 and choice2
         are equal, the result should be "This game is a tie."
    	 
         Make sure to use strcmp for string comparison.
         */
	char * outcome = (char *) malloc(14 * sizeof(char));
	int cmp = 0;
	
	cmp = strcmp(choice1,choice2);
	if(cmp==0){
		strcpy(outcome,"This game is a tie.\n");
	}else if(cmp < 0){
		if(!strcmp(choice2,"scissors")){
			if(!strcmp(choice1,"paper")){
				strcpy(outcome,"Computer wins\n");
			}else{
				strcpy(outcome,"Player wins\n");
			}
		}else{
			strcpy(outcome,"Player wins\n");
		}
	}else{
		if(!strcmp(choice2,"rock")){
			strcpy(outcome,"Computer wins\n");
		}else{
			if(!strcmp(choice1,"rock")){
				strcpy(outcome,"Computer wins\n");
			}else{
				strcpy(outcome,"Player wins\n");
			}
		}
	}
	return outcome;
    }
     
    int main(int argc, char** argv)
    {
        char *userChoice = NULL, *computerChoice = NULL, *outcome = NULL;
     
	userChoice = (char *) malloc(9*sizeof(char));
        getUserChoice(userChoice);
        computerChoice = getComputerChoice();
     
        outcome = compare(userChoice, computerChoice);
     
        printf("You picked %s.\n", userChoice);
        printf("Computer picked %s.\n", computerChoice);
        printf("%s\n", outcome);
     
	free(userChoice);
	free(computerChoice);
	free(outcome);

        return 0;
    }



