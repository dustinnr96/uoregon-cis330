#include <stdio.h>
#include <math.h>
#include "power.h"

double power(double a, double b){
//	return pow(a,b);

	double ret = 1.0;
	
	while(b > 0){
		ret = a * ret;
		b--;
	}
	return ret;
}


