#include <stdio.h>
#include <stdlib.h>
#define ROW 3
#define COL 4

int arrayEqual(int a[ROW][COL], int b[ROW][COL], int m, int n)
{
	for(int i = 0; i < m; i++){
		for(int j = 0; j < n; j++){
			if(a[i][j] != b[i][j]){
				return 0;
			}
		}
	}
	return 1;	
}

int arrayEqual2(int **a, int **b, int m, int n)
{
        for(int i = 0; i < m; i++){
                for(int j = 0; j < n; j++){
                        if(a[i][j] != b[i][j]){
                                return 0;
                        }
                }
        }
        return 1;
}


int main(int argc, const char * argv[])
{
	int a[ROW][COL] = {
		{0, 1, 2, 3},
		{4, 5, 6, 7},
		{8, 9, 10, 11}
	};
	
	int b[ROW][COL] = {
		{0, 1, 2, 3},
		{4, -1, 6, 7},
		{8, 9, 10, 11}
	};

	int **a2;
	int **b2;
	a2 = (int**) malloc(2 * sizeof(int*));
	b2 = (int**) malloc(2 * sizeof(int*));

	for(int i = 0; i < 2; i ++){
		a2[i] = (int*) malloc(3 * sizeof(int));
		b2[i] = (int*) malloc(3 * sizeof(int));
	}

	for(int i = 0; i < 2; i ++){
		for(int j = 0; j < 3; j++){
			a2[i][j] = i * j;
			b2[i][j] = i * j + 1;
		}
	}

	if(arrayEqual(a,b,ROW,COL)){
		printf("The arrays a and b are equal\n");
	}else{
		printf("The arrays a and b are not equal\n");
	}

	if(arrayEqual2(a2,b2,2,3)){
		printf("The arrays a2 and b2 are equal\n");
	}else{
		printf("The arrays a2 and b2 are not equal\n");
	}

	for(int i = 0; i < 2; i ++){
                free(a2[i]);
                free(b2[i]);
        }
	free(a2);
	free(b2);	
	return 0;
}
