#ifndef _VEC2_HPP_
#define _VEC2_HPP_

#include <cmath>

struct vec2 {
        double x;
        double y;

        vec2();
        vec2(const double x, const double y);
        vec2 operator+(const vec2& A) const;
        vec2 operator-(const vec2& A) const;
        vec2 operator*(const double c) const;
        vec2 operator/(const double c) const;

	double dot(const vec2& A) const;
        double operator!() const;
        bool operator==(const vec2& A) const;

};

#endif
