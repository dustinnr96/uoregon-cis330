#include "body.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>



	Body::Body(){
		pos = vec2(0,0);
		vel = vec2(0,0);
		acc = vec2(0,0);
		mass = 0.0;
		fixed = false;
		int red = rand () % 256;
		int blue = rand () % 256;
		int green = rand () % 256;
		color = (red << (4*6)) + (blue << (4*4)) + (green << (4*2)) + 256;
		double position_scaling = 0.0;
	};

	Body::Body(int x, int y){
		pos = vec2(x,y);
		vel = vec2(0,0);
		acc = vec2(0,0);
		mass = 0.0;
		fixed = false;
		int red = rand () % 256;
		int blue = rand () % 256;
		int green = rand () % 256;
		color = (red << (4*6)) + (blue << (4*4)) + (green << (4*2)) + 256;
		double position_scaling = 0.0;
	};

	Body::Body(unsigned int id, int x, int y, double _mass){
		_id = id;
		pos = vec2(x,y);
		vel = vec2(0,0);
		acc = vec2(0,0);
		mass = _mass;
		fixed = false;
		int red = rand () % 256;
		int blue = rand () % 256;
		int green = rand () % 256;
		color = (red << (4*6)) + (blue << (4*4)) + (green << (4*2)) + 256;
	}

	Body::Body(unsigned int id, int x, int y, double _mass, double pos_scale){
		_id = id;
		pos = vec2(x,y);
		vel = vec2(0,0);
		acc = vec2(0,0);
		mass = _mass;
		fixed = false;
		int red = rand () % 256;
		int blue = rand () % 256;
		int green = rand () % 256;
		color = (red << (4*6)) + (blue << (4*4)) + (green << (4*2)) + 256;
		position_scaling = pos_scale;
	}

	unsigned int Body::id(){
		return _id;
	}

	vec2 Body::get_pos(){return pos;}

	vec2 Body::get_vel(){return vel;}

	vec2 Body::get_acc(){return acc;}

	void Body::set_color(int red, int blue, int green, int alpha){
		color = (red << (4*6)) + (blue << (4*4)) + (green << (4*2)) + 256;
	}

	vec2 Body::force(Body & b, double G){
		if(id() != b.id()){
			return force(b.get_pos(), b.get_mass(), G);
		}
		return vec2(0,0);
	}

	vec2 Body::force(vec2 com, double mass, double G){
		vec2 dist(0,0);
		dist.x = pos.x - com.x;
		dist.y = pos.y - com.y;
		double mag = !dist;
		if(mag >= 1){
			double mag_pow = pow(mag,3);
			vec2 force_vec = dist/mag_pow;
			vec2 res = force_vec * (G * mass);
			return res;
		}else{
			return vec2(0,0);
		}
	}

	void Body::update_position(double tdelta){

		pos.x = pos.x + vel.x*tdelta;
		pos.y = pos.y + vel.y*tdelta;
	}

	void Body::update_acceleration(const vec2& acc){
		this->acc = (this->acc + acc);
	}
	void Body::update_velocity(double tdelta){
		vel.x = vel.x + acc.x*tdelta;
		vel.y = vel.y + acc.y*tdelta;
	}
	void Body::update_velocity(double vx, double vy){
		vel.x = vx;
		vel.y = vy;
	}

	void Body::handleEvent(SDL_Event& e){
		if (e.type == SDL_KEYDOWN && e.key.repeat == 0){
			switch( e.key.keysym.sym){

				case SDLK_UP: vel.y -= BODY_VEL; break;
				case SDLK_DOWN: vel.y += BODY_VEL; break;
				case SDLK_LEFT: vel.x -= BODY_VEL; break;
				case SDLK_RIGHT: vel.x += BODY_VEL; break;
			}
		}else if(e.type == SDL_KEYUP && e.key.repeat == 0){

			switch( e.key.keysym.sym){
				case SDLK_UP: vel.y += BODY_VEL; break;
				case SDLK_DOWN: vel.y -= BODY_VEL; break;
				case SDLK_LEFT: vel.x += BODY_VEL; break;
				case SDLK_RIGHT: vel.x -= BODY_VEL; break;
			}
		}
	};

	int Body::get_mass(){
		return mass;
	}

	void Body::move(){
		pos.x += vel.x;

		if (pos.x < 0){pos.x = 800;}else if(pos.x + BODY_W > 800){
			pos.x = 0;
		}

		pos.y += vel.y;
		if ( pos.y < 0) { pos.y = 600;}else if(pos.y + BODY_H > 600){
			pos.y = 0;
		}

		pos.x /= position_scaling;
		pos.y /= position_scaling;
	};

	void Body::render(SDL_Renderer* renderer){
	//	gDotTexture.render(x,y);

		filledCircleColor(renderer, pos.x, pos.y,1/*log2(mass)*/, color);

	};
