#include <string>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <cmath>
#include <climits>
#include "SDL2/SDL.h"
#include "body.hpp"

#include "sim.hpp"

#define SIM_SIZE 2000
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
SDL_Window* gWindow = NULL;
SDL_Surface* gScreenSurface = NULL;
SDL_Renderer* gRenderer = NULL;

int getInt(int low, int high) {
	/* Keep prompting the user for input until they enter a single integer
	 * whose value is between low and high (inclusive).
	 */
	int num = INT_MIN;
	while (num < low || num > high) {
	    std::cin >> num;
	}
	return num;
}


bool init(int width, int height){
	bool success = true;

	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf("Init error: %s\n", SDL_GetError());
		success = false;
	}
	else{
		gWindow = SDL_CreateWindow("SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width,height, SDL_WINDOW_SHOWN);
		if(gWindow == NULL )
		{
			printf("Window not created: %s\n", SDL_GetError());
			success = false;
		}else{
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
			if(gRenderer == NULL ) {
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}else{
				SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0x00);

			}

		}
	}
	return success;
}

void close(){

	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	SDL_Quit();
}

SDL_Surface* loadSurface( std::string path){
	SDL_Surface* optimizedSurface = NULL;
	SDL_Surface* loadedSurface = NULL;
	loadedSurface = SDL_LoadBMP( path.c_str());

	if(loadedSurface == NULL){
		printf( "Unable to load image %s\nError: %s\n", path.c_str(), SDL_GetError() );
	}else{
		optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface->format, NULL);
		if( optimizedSurface == NULL ) {
			printf( "Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		 }

		SDL_FreeSurface(loadedSurface);
	}
	return optimizedSurface;
}

int main(int argc, char *argv[])
{
	//SDL vars
	bool quit = false;
	SDL_Event e;
	int count = 0;
	int width, height, size, G_multiplier, iterations;
	int choice;
	std::cout << "\"Black hole\" sim" << std::endl;
	std::cout << "Which implementation would you like to use?\n 1 - Brute force \n 2 - Barnes-hut" << std::endl;
	choice << getInt(1,2);
	// std::cout << "Enter display width (in pixels 200-2000): ";
	width = 400;
	// std::cout << "Enter display height (in pixels, 200-2000): ";
	height = 400;
	size = 500;
  // std::cout << "Enter simulation size (2-10000000000): ";
  // std::cout << "Enter gravitational force multiplier: (-1 for none, or 0-10000000000): ";
  G_multiplier = 10000;
	// std::cout << "Enter number of timesteps to simulate: (-1 for endless, or 0-10000000000): ";
	iterations = 750;// getInt(-1,INT_MAX);
	if(G_multiplier == -1){
			G_multiplier = 1;
	}
	double G = 6.674 * pow(10,-11) * G_multiplier;

//Sim::Sim(const int width, const int height,  const double step, const int size, const double Gforce){
		Sim sim = Sim(width, height,  1, size, G, 50000);
		// sim.orbit();
		 sim.black_hole();

    if( init(width,height) ){
		while( !quit && (iterations == -1 || count < iterations)){
			while( SDL_PollEvent( &e ) != 0  )
			{
				if( e.type == SDL_QUIT){
					quit = true;

				}
        if( count >= iterations){
					quit = true;
				}
			}
			SDL_SetRenderDrawColor( gRenderer, 0x00, 0x00, 0x00, 0x00 );
			SDL_RenderClear( gRenderer );
			std::cout << "Iteration " << count << " ================================" << std::endl;
			if(choice == 1){
				sim.run(gRenderer);
			}else{
				sim.run_qtree(gRenderer);
			}

			SDL_RenderPresent(gRenderer);
			count ++;
		}


		close();
		return 0;
	}
	return 1;
}
