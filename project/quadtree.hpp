#ifndef _QUADTREE_H_
#define _QUADTREE_H_

#include "vec2.hpp"
#include "body.hpp"

class QuadTree
{
  private:
    QuadTree* nw;
    QuadTree* ne;
    QuadTree* sw;
    QuadTree* se;

    Body * b;

    double mass;
    //top right of frame
    vec2 tr;
    //bottom left of frame
    vec2 bl;
    vec2 center_of_mass;

    bool init_quads();
    void set_mass();
    void set_mass(double m);
    vec2 get_midpoint();
    vec2& get_com();


  public:
    QuadTree();
    QuadTree(double x1, double y1, double x2, double y2);
    ~QuadTree();
    void insert(Body * b1);
    void insert_quad(Body * b1);
    double get_mass();
    double get_area();
    vec2 tree_force(Body & b, double G);
    bool far_away(vec2 x);
    bool is_leaf();
};



#endif _QUADTREE_H_
