#ifndef BODY_HPP_
#define BODY_HPP_


#include "SDL2/SDL.h"
#include "SDL2/SDL2_gfxPrimitives.h"
#include <vector>
#include "vec2.hpp"

class Body {

public:
	static const int BODY_W = 5;
	static const int BODY_H = 5;

	static const int BODY_VEL = 10;

	bool fixed;

	Body();
	Body(int x, int y);
	Body(int x, int y, double _mass);
	Body(unsigned int id, int x, int y, double _mass);
	Body(unsigned int id, int x, int y, double _mass, double pos_scale);
	unsigned int id();
	vec2 get_pos();
	vec2 get_vel();
	vec2 get_acc();
	int get_mass();
	vec2 force(Body & b, double G);
	vec2 force(vec2 com, double mass, double G);
	void update_acceleration(const vec2& acc);

	void update_position(double tdelta);
	void update_velocity(double tdelta);
	void update_velocity(double vx, double vy);
	void set_color(int red, int blue, int green, int alpha);
	void handleEvent(SDL_Event& e);
	void move();
	void render(SDL_Renderer* renderer);

	double position_scaling;
private:
	vec2 pos;
	vec2 vel;
	vec2 acc;
	double mass;
	unsigned int _id;
	unsigned int color;
};

#endif
