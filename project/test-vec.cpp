#include <iostream>
#include "vec2.hpp"
#include <cmath>
int main(int argc, char ** argv[]){

	vec2 v = vec2(1,2);
	std::cout << "v.x = " << v.x << " v.y = " << v.y << std::endl;
	vec2 b = vec2(3,4);
	std::cout << "b.x = " << b.x << " b.y = " << b.y << std::endl;
	vec2 add = v + b;
	std::cout << "add.x = " << add.x << " add.y = " << add.y << std::endl;
	vec2 sub = v - b;
	std::cout << "sub.x = " << sub.x << " sub.y = " << sub.y << std::endl;
	vec2 scal_mult = v * 3;
	std::cout << "v * 2 = " << "<" << scal_mult.x << "," << scal_mult.y << ">" << std::endl;
	vec2 scal_div = v / 2;
	std::cout << "v / 2 = " << "<" << scal_div.x << "," << scal_div.y << ">" << std::endl;
	double dotprod = v.dot(b);
	std::cout << "v dot b = " << dotprod << std::endl;
	double mag = !v;
	double powmag = pow(!v,3);
	std::cout << "|v| = " << mag << " " << powmag << std::endl;
	bool equals = v == b;
	std::cout << "v == b = " << equals << std::endl;
	v = b;
	std::cout << "result of v = b: v.x = " << v.x << " v.y = " << v.y << std::endl;

}
