#include "body.hpp"
#include <iostream>
#include <cmath>

int main(int argc, char ** argv[]){

	double G = 6.67408 * pow(10,-11);
	std::cout << "Body test. G = " << G << std::endl;
	Body b = Body(0,10,10,10.0);

	Body c = Body(1,5,5,10000000.0);

	vec2 distance = b.get_pos() - c.get_pos();
	vec2 inline_acc = (distance/pow(!distance,3) * G * 100000000000000.0);
	vec2 bacc = b.get_acc();
	std::cout << "B a.x = " << bacc.x << " a.y = " << bacc.y << std::endl;

	std::cout << "Inline acc: " << inline_acc.x << ", " << inline_acc.y << std::endl;
	std::vector<Body*> bodies;
	bodies.push_back(&c);
	std::cout << "Body count: " << bodies.size() << std::endl;
	//b.update_acceleration(bodies,G);
	bacc = b.get_acc();

	std::cout << "B a.x = " << bacc.x << " a.y = " << bacc.y << std::endl;
}
