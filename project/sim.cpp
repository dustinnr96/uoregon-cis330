#include "sim.hpp"
#include <string>
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <cmath>

Sim::Sim(const int width, const int height,  const double step, const int size, const double Gforce, double pos_scale):qtree(width,0,0,height){
  id_counter = 0;
  sim_size = size;
  timestep = step;
  sim_width = width;
  sim_height = height;
  G = Gforce;
  position_scaling = pos_scale;
  reverse = false;
}


void Sim::orbit(){
  bodies.push_back((Body(id_counter++,sim_width/2,sim_height/2,1000000)));
 	 bodies[0].fixed = true;

   Body c = Body(id_counter++,.53*sim_width,.45*sim_height,100);
   c.update_velocity(-.13,-.001);

   bodies.push_back(c);
}

void Sim::planet_sim(int planet_count, int small_objects){
  double earth_mass = 5.9722 * pow(10,24);

  for(int i = 0; i < planet_count; i ++){
    double x = rand () % sim_width;
    double y = rand () % sim_height;
    double mass = (rand() % 1000) / 1000.0  * earth_mass;
    bodies.push_back((Body(id_counter++,x,y,mass)));
  }

  for(int i = 0; i < small_objects; i ++){
    double x = rand () % sim_width;
    double y = rand () % sim_height;
    double moon_mass = earth_mass / 81.300570;
    double mass = (rand() % 10) / 10.0 * moon_mass;
    bodies.push_back((Body(id_counter++,x,y,mass)));
  }
}

void Sim::black_hole(){
  bodies.push_back((Body(id_counter++,sim_width/2,sim_height/2,1000000)));
   bodies[0].fixed = true;

   // qtree.insert(&bodies[0]);
   std::vector<Body> circle1;
   circle_fill(id_counter,vec2(.2*sim_width,.2*sim_height),50,sim_size ,bodies);
  //  circle_fill(id_counter,vec2(.8*sim_width,.8*sim_height),50,sim_size/2,bodies);
   int x = 0;
   int y = 0;
   int mass = 0;

}
void Sim::reverse_gravity(){
  reverse = true;

  bodies.push_back((Body(id_counter++,sim_width/2,sim_height/2,1000000)));
   bodies[0].fixed = true;

   // qtree.insert(&bodies[0]);
   std::vector<Body> circle1;

   int x = 0;
   int y = 0;
   int mass = 0;

   for(int i = 1; i < sim_size+1; i ++){
     x = rand () % sim_width;
     y = rand () % sim_height;
     mass = (rand () % 1000);
     bodies.push_back((Body(id_counter++,x,y,mass)));

   }
}
void Sim::sample_sim(){



   // qtree.insert(&bodies[0]);
   std::vector<Body> circle1;

   int x = 0;
   int y = 0;
   int mass = 0;

   for(int i = 1; i < sim_size+1; i ++){
     x = rand () % sim_width;
     y = rand () % sim_height;
     mass = (rand () % 1000);
     bodies.push_back((Body(id_counter++,x,y,mass)));

   }
}
void Sim::run_qtree(SDL_Renderer * renderer){
  int inner = 0;
  int outer = 0;
  qtree = QuadTree(sim_width, 0, 0, sim_height);
  std::for_each(bodies.begin(), bodies.end(),
              [this](Body & b1){
                qtree.insert(&b1);
              });

  std::for_each(bodies.begin(), bodies.end(),
              [&outer, &inner, this, renderer](Body& b1){
                  if(!b1.fixed){
                    b1.update_position(timestep);
                    vec2 tf = qtree.tree_force(b1,G);
                    if(reverse){
                      tf = tf * -1;
                    }
                    b1.update_acceleration(tf);
                    b1.update_velocity(timestep);
                  }
                  b1.render(renderer);
                  outer++;
                }
  );
}

void Sim::circle_fill(unsigned int start_id, vec2 center, int radius, int count, std::vector<Body> & container){
  for(int i = 0; i < count; i++){
    vec2 mag = vec2(radius,radius);
    double x;
    double y;
    int made= 0;
    while(!mag > radius){
      x = ((rand () % radius ) *2  - radius);
      y = ((rand () % radius ) * 2 - radius);
      mag = vec2(x,y);
    }
    x = center.x + x;
    y = center.y + y;
    std::cout << "filled " << i << " in circle." << std::endl;
    vec2 pos = vec2(x,y);
    double mass = (rand() % 10);
    container.push_back(Body(start_id+i,x,y,mass));
  }


}

void Sim::run(SDL_Renderer* renderer){

  int inner = 0;
  int outer = 0;

    std::for_each(bodies.begin(), bodies.end(),
                [&outer, &inner, this, renderer](Body& b1){
                    b1.update_position(timestep);
                    inner = 0;
                    std::for_each(bodies.begin(), bodies.end(),
                                [&b1,&outer, &inner, this, renderer](Body b2){
                                  if(b2.id() != b1.id() && !b1.fixed){
                                    vec2 dist(0,0);
                                    dist.x = b1.get_pos().x - b2.get_pos().x;
                                    dist.y = b1.get_pos().y - b2.get_pos().y;
                                    double mag = !dist;
                                    if(mag < 1){
                                      b1.update_acceleration(vec2(0,0));
                                    }else{
                                      double mag_pow = pow(mag,3);
                                      vec2 force_vec = dist/mag_pow;
                                      vec2 res = force_vec * (G * b2.get_mass());
                                      b1.update_acceleration(res*-1);
                                      if(reverse){
                                        b1.update_acceleration(res);
                                      }else{
                                          b1.update_acceleration(res*-1);
                                      }
                                    }
                                  }
                                    inner++;
                    });
                    b1.update_velocity(timestep);
                    b1.render(renderer);
                    outer++;
                  }
    );


//  }
}
