#ifndef SIM_HPP_
#define SIM_HPP_

#include "body.hpp"
#include "quadtree.hpp"

class Sim{

public:
  int iterations;
  int sim_size;
  double G;
  double timestep;
  double position_scaling;
  int sim_width;
  int sim_height;
  bool reverse;

  Sim(const int width, const int height,  const double step, const int size, const double Gforce, double pos_scale);
	void init_sim();
  void sample_sim();
  void black_hole();
  void planet_sim(int planet_count, int small_objects);
  void reverse_gravity();
  void orbit();
  void circle_fill(unsigned int start_id, vec2 center, int radius, int count, std::vector<Body> & container);
  void run(SDL_Renderer* renderer);
  void run_qtree(SDL_Renderer * renderer);

private:
  unsigned int id_counter;
	std::vector<Body> bodies;
  QuadTree qtree;
};
#endif
