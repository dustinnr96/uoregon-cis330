#include <string>
#include <iostream>
#include "SDL2/SDL.h"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
SDL_Window* gWindow = NULL;
SDL_Surface* gScreenSurface = NULL;
SDL_Renderer* gRenderer = NULL;
bool init(){
	bool success = true;
	
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf("Init error: %s\n", SDL_GetError());
		success = false;
	}
	else{
		gWindow = SDL_CreateWindow("SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800,600, SDL_WINDOW_SHOWN);
		if(gWindow == NULL )
		{
			printf("Window not created: %s\n", SDL_GetError());
			success = false;
		}else{
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );
			if(gRenderer == NULL ) {
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}else{
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF);

			}
		/*	
				gScreenSurface = SDL_GetWindowSurface(gWindow);
			if(gScreenSurface == NULL){
				printf("Surface not created: %s\n", SDL_GetError());
				success = false;
			}*/
		}
	}
	return success;
}

void close(){
	
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	SDL_Quit();
}

SDL_Surface* loadSurface( std::string path){
	SDL_Surface* optimizedSurface = NULL;
	SDL_Surface* loadedSurface = NULL;
	std::cout <<" Loading file at " << path << std::endl;
	loadedSurface = SDL_LoadBMP( path.c_str());
	std::cout << "Surface null? " << (int)(loadedSurface == NULL) << std::endl;
	std::cout << "gSurface null? " << (int)(gScreenSurface == NULL) << std::endl;	
	if(loadedSurface == NULL){
		printf( "Unable to load image %s\nError: %s\n", path.c_str(), SDL_GetError() );
	}else{
		optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface->format, NULL);
		if( optimizedSurface == NULL ) { 
			printf( "Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		 }
		
		SDL_FreeSurface(loadedSurface);
	}
	return optimizedSurface;
}

/* Moving Rectangle */
int main(int argc, char *argv[])
{
	bool quit = false;

	SDL_Event e;

        if( init() ){
		while( !quit){
			while( SDL_PollEvent( &e ) != 0)
			{
				if( e.type == SDL_QUIT){
					quit = true;
				
				}
				//clear screen
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( gRenderer );
				
				SDL_Rect fillRect = { SCREEN_WIDTH / 4, SCREEN_HEIGHT / 4, SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2 };
				SDL_SetRenderDrawColor (gRenderer, 0xFF, 0x00, 0x00, 0xFF );
				SDL_RenderFillRect(gRenderer, &fillRect);

				SDL_RenderPresent( gRenderer );
			}
		}
//		SDL_Delay(2000);
		close();
		return 0;
	} 
	return 1;
}

