#include <string>
#include <iostream>
#include "SDL2/SDL.h"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
SDL_Window* gWindow = NULL;
SDL_Surface* gScreenSurface = NULL;

bool init(){
	bool success = true;
	
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf("Init error: %s\n", SDL_GetError());
		success = false;
	}
	else{
		gWindow = SDL_CreateWindow("SDL", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800,600, SDL_WINDOW_SHOWN);
		if(gWindow == NULL )
		{
			printf("Window not created: %s\n", SDL_GetError());
			success = false;
		}else{
			gScreenSurface = SDL_GetWindowSurface(gWindow);
			if(gScreenSurface == NULL){
				printf("Surface not created: %s\n", SDL_GetError());
				success = false;
			}
		}
	}
	return success;
}

void close(){
	
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	
	SDL_Quit();
}

SDL_Surface* loadSurface( std::string path){
	SDL_Surface* optimizedSurface = NULL;
	SDL_Surface* loadedSurface = NULL;
	std::cout <<" Loading file at " << path << std::endl;
	loadedSurface = SDL_LoadBMP( path.c_str());
	std::cout << "Surface null? " << (int)(loadedSurface == NULL) << std::endl;
	std::cout << "gSurface null? " << (int)(gScreenSurface == NULL) << std::endl;	
	if(loadedSurface == NULL){
		printf( "Unable to load image %s\nError: %s\n", path.c_str(), SDL_GetError() );
	}else{
		optimizedSurface = SDL_ConvertSurface(loadedSurface, gScreenSurface->format, NULL);
		if( optimizedSurface == NULL ) { 
			printf( "Unable to optimize image %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		 }
		
		SDL_FreeSurface(loadedSurface);
	}
	return optimizedSurface;
}

/* Moving Rectangle */
int main(int argc, char *argv[])
{
	bool quit = false;

	SDL_Event e;

        if( init() ){
		SDL_Surface* bg = loadSurface("/home/dustin/cis330/uoregon-cis330/project/background.bmp"); 
		SDL_Rect stretchRect;
		stretchRect.x = 0;
		stretchRect.y = 0;
		stretchRect.w = SCREEN_WIDTH;
		stretchRect.h = SCREEN_HEIGHT;
		SDL_BlitScaled( bg, NULL, gScreenSurface, &stretchRect );
		while( !quit){
			while( SDL_PollEvent( &e ) != 0)
			{
				if( e.type == SDL_QUIT){
					quit = true;
				
				}
			}
		}
//		SDL_Delay(2000);
		close();
		return 0;
	} 
	return 1;
}

