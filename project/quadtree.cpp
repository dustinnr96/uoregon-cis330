#include "quadtree.hpp"

#include <vector>
#include <iostream>
#include <algorithm>

QuadTree::QuadTree():tr(),bl(),center_of_mass(){
  // std::cout << "initing quadtree: " << "tr: " << 0 << "," << 0 << " bl: " << 0 << "," << 0 << std::endl;
   nw= NULL;
   ne= NULL;
   sw= NULL;
   se= NULL;
   b = NULL;
   mass = 0.0;
}


QuadTree::QuadTree(double x1, double y1, double x2, double y2):tr(x1,y1),bl(x2,y2),center_of_mass(){
  // std::cout << "initing quadtree: " << "tr: " << x1 << "," << y1 << " bl: " << x2 << "," << y2 << std::endl;
   nw= NULL;
   ne= NULL;
   sw= NULL;
   se= NULL;
   b = NULL;
   mass = 0.0;
}

vec2 QuadTree::get_midpoint(){
  return vec2(bl.x+(tr.x-bl.x)/2,tr.y+(bl.y-tr.y)/2);
}

  QuadTree::~QuadTree(){
    if(nw != NULL){
      free(nw);
    }else if(ne != NULL){
      free(ne);
    }else if(se != NULL){
      free(se);
    }else if(sw != NULL){
      free (sw);
    }
  }
bool QuadTree::init_quads(){
  if(nw == NULL){
    // std::cout << "Initing quads." << std::endl;
    vec2 midpoint = get_midpoint();
    int width = (tr.x-bl.x);
    int height = (bl.y-tr.y);

    int mid_width = bl.x+width/2;
    int mid_height = tr.y+height/2;

    if(width  == 1 && height == 1){
    //  std::cout << "uh oh collision" << std::endl;
      return false;
    }else if(width == 1){
      // std::cout << "nw: ";
      nw = new QuadTree(tr.x, tr.y, bl.x, mid_height);
      // std::cout << "ne: ";
      ne = new QuadTree();
      // std::cout << "sw: ";
      sw = new QuadTree(tr.x, mid_height, bl.x, bl.y);
      // std::cout << "se: ";
      se = new QuadTree();
    }else if(height == 1){
      // std::cout << "nw: ";
      nw = new QuadTree(mid_width, tr.y, bl.x, bl.y);
      // std::cout << "ne: ";
      ne = new QuadTree(tr.x, tr.y, mid_width, bl.y);
      // std::cout << "sw: ";
      sw = new QuadTree();
      // std::cout << "se: ";
      se = new QuadTree();
    }else{
      // std::cout << "nw: ";
      nw = new QuadTree(mid_width, tr.y, bl.x, mid_height);
      // std::cout << "ne: ";
      ne = new QuadTree(tr.x, tr.y, mid_width, mid_height);
      // std::cout << "sw: ";
      sw = new QuadTree(mid_width, mid_height, bl.x, bl.y);
      // std::cout << "se: ";
      se = new QuadTree(tr.x, mid_height, mid_width, bl.y);
    }
    return true;
  }
}


void QuadTree::set_mass(double m){
  mass = m;
}

void QuadTree::set_mass(){
  if(nw == NULL){
    if(b == NULL){
      mass = 0.0;
      center_of_mass = vec2(0,0);
    }else{
      mass = b->get_mass();
      center_of_mass = b->get_pos();
    }
  }else{
    vec2 nw_com = nw->get_com();
    double nw_mass = nw->get_mass();
    vec2 ne_com = ne->get_com();
    double ne_mass = ne->get_mass();
    vec2 sw_com = sw->get_com();
    double sw_mass = sw->get_mass();
    vec2 se_com = se->get_com();
    double se_mass = se->get_mass();

    double com_x = nw_com.x*nw_mass + ne_com.x*ne_mass + sw_com.x*sw_mass + se_com.x*se_mass;
    double com_y = nw_com.y*nw_mass + ne_com.y*ne_mass + sw_com.y*sw_mass + se_com.y*se_mass;
    mass = nw_mass + ne_mass + sw_mass + se_mass;
    center_of_mass = vec2(com_x/mass, com_y/mass);
  }
}

void QuadTree::insert_quad(Body * b1){
  vec2 b1_pos = b1->get_pos();
  vec2 midpoint = get_midpoint();

  if(b1_pos.x >= bl.x && b1_pos.x <= midpoint.x &&
      b1_pos.y >= tr.y && b1_pos.y <= midpoint.y){
    nw->insert(b1);
  }else if(b1_pos.x <= tr.x && b1_pos.x > midpoint.x &&
      b1_pos.y >= tr.y && b1_pos.y <= midpoint.y){
    ne->insert(b1);
  }else if(b1_pos.x >= bl.x && b1_pos.x <= midpoint.x &&
      b1_pos.y <= bl.y && b1_pos.y > midpoint.y){
    sw->insert(b1);
  }else if(b1_pos.x <= tr.x && b1_pos.x > midpoint.x &&
      b1_pos.y <= bl.y && b1_pos.y > midpoint.y){
    se->insert(b1);
  }else{
  }
}

void QuadTree::insert(Body * b1){
  if(b == NULL){
    if(nw == NULL){

      b = b1;
      set_mass();
    }else{
        insert_quad(b1);
        set_mass();
    }
  }else{
    if(nw == NULL){
      bool quad_init = init_quads();
      if(quad_init){
        insert_quad(b1);
        insert_quad(b);
        b = NULL;
        set_mass();
      }else{
      }

    }else{
    }
  }

}

double QuadTree::get_area(){
  return bl.y * tr.x;
}

double QuadTree::get_mass(){

  return mass;
}

vec2& QuadTree::get_com(){
  return center_of_mass;
}

bool QuadTree::is_leaf(){
  return nw == NULL && ne == NULL && sw == NULL && se == NULL;
}

bool QuadTree::far_away(vec2 x){

  return (std::max(tr.x - bl.x, bl.y - tr.y) / (!(get_com() - x))) < 1;
}

vec2 QuadTree::tree_force(Body & b1, double G){
  // std::cout << "tree force" << std::endl;
  if(is_leaf() || far_away(b1.get_pos())){
      //use this nodes CoM, and mass to compute gravitational force
      //return b.force(get_com(), get_mass(), G);

      if(is_leaf()){
        vec2 f = b->force(b1,G);
        return f;
      }else{
        vec2 f = b1.force(center_of_mass, mass, G) * -1;
        return f;
      }

  }else{
    vec2 force_sum = vec2(0,0);
      //std::cout << "calculating internal node force" << std::endl;
    if(nw != NULL && nw->get_mass() > 0){
        force_sum = (force_sum + nw->tree_force(b1,G));
    }
    if(ne != NULL && ne->get_mass() > 0){
        force_sum = (force_sum + ne->tree_force(b1,G));
    }
    if(sw != NULL && sw->get_mass() > 0){
        force_sum = (force_sum + sw->tree_force(b1,G));
    }
    if(se != NULL && se->get_mass() > 0){
        force_sum = (force_sum + se->tree_force(b1,G));
    }
    // std::cout << "f.x = " << force_sum.x << " f.y = " << force_sum.y << std::endl;
    return force_sum;

  }
}
