#include "vec2.hpp"

	vec2::vec2(){};
        vec2::vec2(const double x, const double y):x(x), y(y){};

        vec2  vec2::operator+(const vec2& A)const {
                return  vec2(A.x+x, A.y+y);
        };

        vec2 vec2::operator-(const vec2& A) const{
                return vec2(x-A.x, y-A.y);
        };

        vec2 vec2::operator*(const double c) const{
                return vec2(x*c,y*c);
        };

	vec2 vec2::operator/(const double c) const{
		if(c == 0){
			return vec2(0,0);
		}else{
			return vec2(x/c,y/c);
		}
	}

        double vec2::dot(const vec2& A) const{
                return x*A.x + y*A.y;
        };

        double vec2::operator!() const{
                return sqrt(pow(x,2)+pow(y,2));
        };
/*default?      vec2& operator=(const vec2& A){
                x = A.x;
                y = A.y;
                return *this;
        }*/

        bool vec2::operator==(const vec2& A) const{
                return (x == A.x && y == A.y);
        };
