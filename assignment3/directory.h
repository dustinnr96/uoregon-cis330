#ifndef DIRECTORY_H_
#define DIRECTORY_H_

typedef struct people {
        char * name;
        char * number;

} person;

typedef struct node node;
struct node {
	person * p; 
	node * next;
};


void add_contact(char * name,  char * number, node *contact_list);
int remove_contact(int num, node ** contact_list);
void deallocate_person(person * p);
void free_node(node * n);
void free_list(node * n);
void print_person(person * p);
void print_contact_list(node * list);

#endif
