#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "directory.h"

void removeNewLines(char * str){
	
	for(int i = 0; i < strlen(str); i++){
		if (str[i] == '\n'){
			str[i] = ' ';
		}
	}
}
void clearInputBuffer() {
  while ( getchar() != '\n' );
}


int getInt(const char* msg, int low, int high) {
	/* Keep prompting the user for input until they enter a single integer
	 * whose value is between low and high (inclusive).
	 */
	int numInts = 0, num = 0;
	while (numInts != 1 || num < low || num > high) {
	    printf("%s",msg);
	    numInts = scanf("%d", &num);  // returns the number of integers read from input
	    clearInputBuffer();
	}
	return num;
}

void menu_prompt(node ** list){

	int choice = 0;
	int delete_choice = 0;
	while(1){
		printf("Please choose an option (-1 to quit):\n\t1. Insert a new entry.\n\t2. Delete an entry.\n\t3. Display current directory.\n");
		choice = getInt("> ", INT_MIN, INT_MAX);
		if(choice == 1){
			char * name = (char *) malloc(51 * sizeof(char));
			char * number = (char *) malloc(21 * sizeof(char));
			printf("Enter a name:\n");
			fgets(name,51,stdin);
			removeNewLines(name);
			printf("Enter a number:\n");
			fgets(number,21,stdin);
			removeNewLines(number);
			add_contact(name,number,*list);
			free(name);
			free(number);
		}else if(choice == 2){
			print_contact_list(*list);
			printf("Enter index number of entry to delete:\n");
			delete_choice = getInt("> ",1,INT_MAX);
			remove_contact(delete_choice, list);
		}else if(choice == 3){
			print_contact_list(*list);
		}else if(choice == -1){
			break;
		}else{
			printf("Invalid choice.\n");
		}
	}

}


int main(int argc, char** argv){
	node * contact_list = (node*) malloc(sizeof(node));
	contact_list->p = NULL;
	contact_list->next =NULL;
	menu_prompt(&contact_list);

	free_list(contact_list);
	return 0;
}
