#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "directory.h"

void add_contact(char * name,  char * number, node *contact_list){
   node * current = contact_list;

  while(current->next != NULL ){
			current = current->next;
	}
	current->p = (person *) malloc(sizeof(person));
	current->p->name = (char *) malloc(strlen(name) * sizeof(char));
	strcpy(current->p->name,name);


	current->p->number = malloc(strlen(name) * sizeof(char));
	 strcpy(current->p->number,number);

	current->next = (node *) malloc(sizeof(node));
	current->next->next = NULL;
	current->next->p = NULL;
}

int remove_contact(int num, node ** contact_list){
 node * current = *contact_list;
 node * previous=NULL;
  int idx = 1;
  if(current->p == NULL){
		return 0;
	}

  while(current->next != NULL && idx < num){
		idx++;
		previous = current;
		current = current->next;
	}

	if(current -> next == NULL){
		return 0;
	}
	
		if(previous != NULL){
				previous->next = current->next;
		}else{
			*contact_list = current->next;
		}
		free_node(current);

	return 1;
}


void deallocate_person(person * p){
	if(p != NULL){
		free(p->name);
		free(p->number);
		free(p);
	}
}

void free_node(node * n){
	if(n != NULL){
		n->next = NULL;
		if(n->p != NULL){
			deallocate_person(n->p);
			n->p = NULL;
		}
		free(n);
	}
}

void free_list(node *n){
	if(n != NULL){
		if(n->next != NULL){
			free_list(n->next);
		}

		free_node(n);
	}
}

void print_person(person * p){

        printf("Name: %s\n",p->name);

        printf("Phone Number: %s\n",p->number);
}

void print_contact_list(node * list){
   node * current = list;
	
 printf("\nContacts\n");
printf("==========\n");
	int counter = 1;
  while(current->next != NULL){
		if(current->p != NULL){
			printf("%d. %s- %s\n",counter,current->p->name,current->p->number);
			counter+=1;
		}
		current = current->next;
	}
}


